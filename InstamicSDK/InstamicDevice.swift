//
//  InstamicDevice.swift
//  Instamic
//
//  Created by Jordan Kusic on 3.2.16..
//  Copyright © 2016. AdvancedSoft. All rights reserved.
//

import Foundation
import CoreBluetooth

public enum RecordingState {
    case IDLE
    case PREPARING //prepraring to start recording
    case RECORDING //recording
    case SAVING    //saving recorded,
    case SETTING_AUTOGAIN    //saving recorded
    case MONITORING    //monitoring recorded
}
public enum CommandState {
    case IDLE
    case WAITING_RESPONSE //waiting response from previous command
}
open class InstamicDevice {
    
    static let DEVICE_TYPE_iAP2:UInt8 = 2; //iAp2
    //command codes
    static let CMD_NONE:UInt8                  = 0;
    static let CMD_PING:UInt8                  = 34;
    static let CMD_RESPONSE_OK:UInt8           = 38;
    static let CMD_RESPONSE_ERROR:UInt8        = 39;
    static let CMD_AUTO_GAIN:UInt8             = 40;
    static let CMD_GAIN:UInt8                  = 41;
    static let CMD_RECORDING_MODE:UInt8        = 42;
    static let CMD_SAMPLE_RATE:UInt8           = 43;
    static let CMD_REMAINING_SPACE:UInt8       = 45;
    static let CMD_VU_METER:UInt8              = 46;
    static let CMD_GET_REMAINING_SPACE:UInt8   = 45;
    static let CMD_BUZZER_ONOFF:UInt8          = 47;
    static let CMD_RENAME_FILE:UInt8           = 48;
    static let CMD_DELETE_FILE:UInt8           = 49;
    
    static let CMD_FILE_TRANSVER_START:UInt8   = 50;
    static let CMD_FILE_TRANSVER_STOP:UInt8    = 51;
    static let CMD_GET_FILE_LIST:UInt8         = 52;
    static let CMD_START:UInt8                 = 53;
    static let CMD_STOP:UInt8                  = 54;
    static let CMD_GET_SETTINGS:UInt8          = 55;
    static let CMD_GET_BAT_LEVEL:UInt8         = 56;
    static let CMD_SET_FILTER:UInt8            = 57;
    static let CMD_START_SEMPLING:UInt8        = 58;
    static let CMD_VERSION_INFO:UInt8          = 59;
    static let CMD_STATUS:UInt8                = 60;
    static let CMD_SET_DEVICE_NAME:UInt8       = 61;
    static let CMD_SHUTDOWN:UInt8              = 62;
    
    static let CMD_STREAM_ENABLE:UInt8         = 63;
    static let CMD_STREAM_CONNECT:UInt8        = 64;
    
    public var name:String?
    public var connected:Bool
    public var id:NSUUID
    public var pro:Bool
    public var recordingState:RecordingState = RecordingState.IDLE
    public var firmawareVersion:String?
    public var perhipheral:CBPeripheral?
    public var cmdCharactersitics:CBCharacteristic? // command characteristic
    public var settings:DeviceSettings = DeviceSettings()
    public var mainVersionNumber:UInt8
    public var mediumVersionNumber:UInt8
    public var minorVersionNumber:UInt8
    public var lastCommandSent:UInt8
    public var lastParametersSent:[UInt8]
    public var commandState:CommandState = CommandState.IDLE
    public var lastCommandReceived:UInt8
    public var streamingEnabled:Bool
    
    
    public convenience init(id:NSUUID, name:String?,connected:Bool,pro:Bool) {
        self.init(id:id,name: name,connected: connected,pro: pro,recordingState: RecordingState.IDLE)
    }
    
    public init(id:NSUUID, name:String?,connected:Bool,pro:Bool,recordingState:RecordingState) {
        self.name = name
        self.connected = connected
        self.id = id
        self.pro = pro;
        mainVersionNumber = UInt8.min
        mediumVersionNumber = UInt8.min
        minorVersionNumber = UInt8.min
        lastCommandSent = InstamicDevice.CMD_NONE
        lastCommandReceived = InstamicDevice.CMD_NONE
        self.streamingEnabled = false
        self.lastParametersSent = [UInt8]()
    }
    // minutes + 100*hours + 10000*(year-2000) + 1000000*day + 100000000*month
    public static func getTimestamp() -> UInt32 {
        let date = Date();
        let calendar = Calendar.current;
        let unitFlags:NSCalendar.Unit = [.day,.month,.year,.hour,.minute]
        let components:DateComponents = (calendar as NSCalendar).components(unitFlags, from:date)
        
        let monthComp = 100000000*components.month!
        let dayComp = 1000000*components.day!
        let yearComp =  10000*(components.year! - 2000)
        let result = UInt32(components.minute! + 100 * components.hour! + yearComp + dayComp  + monthComp)
        NSLog("Generated time stamp \(result) for year:\(components.year!) month:\(components.month!) day:\(components.day!) hour:\(components.hour!) minutes:\(components.minute!)")
        return result
    }
    
    public static  func countNonZeroBytes(rawData:[UInt8],startIndex:Int)->UInt8 {
        var counter:UInt8 = 0;
        let maxRawDataIndex = rawData.count-1;
        for i in startIndex...maxRawDataIndex {
            if (rawData[i] > 0) {
                counter += 1;
            } else {
                break;
            }
        }
        return counter;
    }
    
    public static func cobsEncode(rawData:[UInt8])->[UInt8] {
        var encodedData = [UInt8]();
        encodedData.append(1 + self.countNonZeroBytes(rawData: rawData, startIndex: 0))
        let maxRawDataIndex = rawData.count-1;
        for i in 0...maxRawDataIndex {
            if (rawData[i] > 0) {
                encodedData.append(rawData[i])
            } else {
                encodedData.append(1 + self.countNonZeroBytes(rawData: rawData, startIndex: i+1))
            }
        }
        return encodedData;
    }
    
    public static func cobsDecode(rawData:[UInt8])->[UInt8] {
        var decodedData = [UInt8]();
        var nonZeroCounter:UInt8  = 0
        let decrementer:UInt8 = 1;
        let maxRawDataIndex = rawData.count-1;
        for i in 0...maxRawDataIndex {
            if (nonZeroCounter > 0) {
                decodedData.append(rawData[i]);
                nonZeroCounter -= 1;
            } else {
                if (i > 0) {
                    decodedData.append(0);
                }
                nonZeroCounter = rawData[i] - decrementer;
            }
        }
        return decodedData;
    }
    
    public static  func calculateChecksum(rawData:[UInt8])->UInt8 {
        var checkSum:UInt8 = 0;
        let rawDataMaxIndex=rawData.count-1;
        for  i in 0...rawDataMaxIndex {
            let result = UInt8.addWithOverflow(checkSum, rawData[i])
            checkSum = result.0
        }
        checkSum = ~checkSum;
        return checkSum;
    }
    
    //sample to percentage
    public static func sampletoToPct(sample:UInt32, resolution:UInt8) ->Double {
        let resol:Double = Double(resolution);
        let logNorm =  log10(0.5) * resol
        let vu:Double = Double(sample)
        let norm:Double = log10(0.001) / logNorm
        let x:Double = vu / pow(2.0, resol - 1.0)
        let a:Double = 1.0 - norm
        let b:Double = 1.0 / norm
        let normLog:Double = log10(x) / logNorm
        let result = ((1.0-normLog) - a) * b
        if (result < 0.0) {
            return 0.0
        }
        return result
    }
    
    public static func sampleToDbfs(sample:UInt32, resolution:UInt8) ->Double {
        if(sample != 0)
        {
            let one:UInt32 = 1;
            let resol:UInt32 = UInt32(resolution);
            let shifted = one << (resol - 1)
            return 20.0 * log10((Double(sample)) / (Double(shifted)));
        }
        else
        {
            return 20.0 * log10(0.5) * Double(resolution)
        }
    }
    
    public static func calculateFreeTimeInSeconds(remainingSpace:UInt32! , sampleRate:UInt8!, stereo:Bool ) -> UInt32! {
        var result:UInt32! = 0;
        var divader:UInt32! = 48 * 3;
        if (sampleRate == 255) {
            divader = 96 * 3;
        }
        if (stereo) {
            divader = divader * 2
        }
        result = remainingSpace / divader;
        return result;
    }
    
    
    public static func composeMessage(cmd:UInt8, parameters:[UInt8])->NSData {
        var data = [UInt8]();
        data.append(0);
        //TODO: dinamic creation of len
        //let two:UInt16 = 2;
        //var length:UInt16  = UInt16(parameters.count) + two;
        var packet = [UInt8]();
        packet.append(UInt8(parameters.count + 1));
        packet.append(0);
        packet.append(cmd);
        packet += parameters;
        packet.append(InstamicDevice.calculateChecksum(rawData: packet))
        data += InstamicDevice.cobsEncode(rawData: packet)
        return NSData(bytes: data, length: data.count);
    }
    
    func sendCommand(cmd:UInt8, params:[UInt8]) {
        if (self.perhipheral != nil) {
            if commandState == CommandState.IDLE {
                let content = InstamicDevice.composeMessage(cmd: cmd, parameters: params)
                self.perhipheral?.writeValue(content as Data, for: cmdCharactersitics!, type: CBCharacteristicWriteType.withResponse)
                self.lastCommandSent = cmd
                self.lastParametersSent = params
                self.commandState = CommandState.WAITING_RESPONSE
                NSLog("Command sent:\(cmd)")
            } else {
                NSLog("Error:cant send command! Device is not in IDLE mode")
            }
        } else {
            NSLog("Error: There is no activ peripheral")
        }
    }
    
    public func sendGetSettings() {
        self.sendCommand(cmd: InstamicDevice.CMD_GET_SETTINGS,params:[UInt8]())
    }
    
    
    public func sendGetRemainingSpace() {
        self.sendCommand(cmd: InstamicDevice.CMD_REMAINING_SPACE,params:[UInt8]())
    }
    
    public func sendStart() {
        var timestamp = InstamicDevice.getTimestamp()
        let count = MemoryLayout<UInt32>.size
        let bytePtr = withUnsafePointer(to: &timestamp) {
            $0.withMemoryRebound(to: UInt8.self, capacity: count) {
                UnsafeBufferPointer(start: $0, count: count)
            }
        }
        let byteArray = Array(bytePtr)
        self.sendCommand(cmd: InstamicDevice.CMD_START,params:byteArray)
    }
    
    
    public func sendStop() {
        self.sendCommand(cmd: InstamicDevice.CMD_STOP,params:[UInt8]())
    }
    
    public func sendEnableAutoGain() {
        self.sendCommand(cmd: InstamicDevice.CMD_AUTO_GAIN,params:[UInt8.max])
        self.settings.autoGain = true
    }
    
    public func sendDisableAutoGain() {
        self.sendCommand(cmd: InstamicDevice.CMD_AUTO_GAIN,params:[UInt8.min])
        self.settings.autoGain = false
    }
    
    public func sendGetBatterylevel() {
        self.sendCommand(cmd: InstamicDevice.CMD_GET_BAT_LEVEL,params:[UInt8]())
    }
    
    public func sendEnableBuzzer() {
        self.sendCommand(cmd: InstamicDevice.CMD_BUZZER_ONOFF,params:[UInt8.max])
        self.settings.buzzer = true
    }
    
    public func sendDisableBuzzer() {
        self.sendCommand(cmd: InstamicDevice.CMD_BUZZER_ONOFF,params:[UInt8.min])
        self.settings.buzzer = false
    }
    
    public func sendRecordingMode( mode:UInt8) {
        self.sendCommand(cmd:InstamicDevice.CMD_RECORDING_MODE,params:[mode])
        self.settings.recordingMode = mode
    }
    public func sendSampleRate( rate:UInt8) {
        self.sendCommand(cmd:InstamicDevice.CMD_SAMPLE_RATE,params:[rate])
        self.settings.sampleRate = rate
    }
    
    public func sendEnableVuMetter() {
        self.sendCommand(cmd: InstamicDevice.CMD_VU_METER,params:[UInt8.max])
    }
    
    public func sendDisableVuMetter() {
        self.sendCommand(cmd: InstamicDevice.CMD_VU_METER,params:[UInt8.min])
    }
    
    public func sendGetStatus() {
        self.sendCommand(cmd: InstamicDevice.CMD_STATUS,params:[UInt8]())
    }
    
    public func sendShutdown() {
        self.sendCommand(cmd: InstamicDevice.CMD_SHUTDOWN,params:[UInt8]())
    }
    
    public func sendGetVersionInfo() {
        self.sendCommand(cmd: InstamicDevice.CMD_VERSION_INFO,params:[UInt8]())
    }
    
    public func sendSetGain(gain:UInt8) {
        self.sendCommand(cmd: InstamicDevice.CMD_GAIN,params:[gain])
        self.settings.gain = gain
    }
    
    public func sendChangeName(newName:String?) {
        self.sendCommand(cmd: InstamicDevice.CMD_SET_DEVICE_NAME,params:[UInt8](newName!.utf8))
        self.name = newName
    }
    
    public func streamEnable(mack:[UInt8]) {
        let tempParams:[UInt8] = [UInt8.max]
        self.sendCommand(cmd: InstamicDevice.CMD_STREAM_ENABLE,params:tempParams)
        //self.streamingEnabled = true
    }
    public func streamDisable() {
        self.sendCommand(cmd: InstamicDevice.CMD_STREAM_ENABLE,params:[UInt8.min])
        //self.streamingEnabled = false
    }
    public func streamConnect() {
        self.sendCommand(cmd: InstamicDevice.CMD_STREAM_CONNECT,params:[InstamicDevice.DEVICE_TYPE_iAP2])
    }
    public func streamDisconnect() {
        self.sendCommand(cmd: InstamicDevice.CMD_STREAM_CONNECT,params:[UInt8.min])
    }
    
    public func ping() {
        self.sendCommand(cmd: InstamicDevice.CMD_PING, params: [UInt8]());
    }
    
    func onCommandExecuted() {
        NSLog("Last command sent \(lastCommandSent) executed")
        switch lastCommandSent {
        case InstamicDevice.CMD_STREAM_ENABLE:
            if lastParametersSent.count > 0  {
                if lastParametersSent[0] == UInt8.max {
                    NSLog("Device Monitoring is enabled")
                    self.streamingEnabled = true;
                    //sending start command
                    sendStart()
                } else if lastParametersSent[0] == UInt8.min {
                    NSLog("Device Monitoring is enabled")
                    self.streamingEnabled = false;
                    if recordingState != RecordingState.IDLE {
                        sendStop()
                    } else {
                        sendDisableVuMetter()
                    }
                }
            }
            break
            
        default:
            NSLog("No handler for command execution")
            break;
        }
    }
    
    func onCommandError() {
        NSLog("Command \(lastCommandSent) execution failed")
        commandState = CommandState.IDLE
        sendGetSettings()
    }
}
