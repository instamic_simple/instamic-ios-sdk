//
//  InstamicSDK.h
//  InstamicSDK
//
//  Created by Jordan Kusic on 7/17/17.
//  Copyright © 2017 Jordan Kusic. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for InstamicSDK.
FOUNDATION_EXPORT double InstamicSDKVersionNumber;

//! Project version string for InstamicSDK.
FOUNDATION_EXPORT const unsigned char InstamicSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InstamicSDK/PublicHeader.h>


