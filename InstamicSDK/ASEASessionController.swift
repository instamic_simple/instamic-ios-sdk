//
//  ASEASessionController.swift
//
//  Created by Jordan Kusic on 10.8.16..
//  Copyright © 2016. AdvancedSoft. All rights reserved.
//

import UIKit
import ExternalAccessory
import AudioToolbox
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


public class ASEAState {
    var buffers = [AudioQueueBufferRef?]()
    var queue:AudioQueueRef?
    var totalBytesReaded = 0
    var format:AudioStreamBasicDescription = AudioStreamBasicDescription(
        mSampleRate: 32000.0,
        mFormatID:kAudioFormatLinearPCM,
        mFormatFlags:  kLinearPCMFormatFlagIsSignedInteger,
        mBytesPerPacket: 2,
        mFramesPerPacket: 1,
        mBytesPerFrame: 2,
        mChannelsPerFrame:1,
        mBitsPerChannel:16,
        mReserved:0
    )
    var playing:Bool = false
    var usedBuffers = 0
    
}

class ASEASessionController: NSObject,EAAccessoryDelegate,StreamDelegate {
    static let BUFFERS_COUNT = 52
    static let BUFFER_SIZE:UInt32 = 1024
    var accessory:EAAccessory? = nil
    var eaProtocol:String? = nil
    var eaSession:EASession? = nil
    var sessionStatus:ASEAState = ASEAState()
    private let concurrentBufferQueue = DispatchQueue(label: "buffer_barier")
    
    static let sharedInstance = ASEASessionController()
    private override init() {
        
    }
    
    var _writeDate:NSMutableData? = nil
    func settupForAccessory(_ accessory:EAAccessory?,eaP:String?) {
        self.accessory = accessory
        self.eaProtocol = eaP
    }
    
    func appendBuffer(outBuffer:AudioQueueBufferRef) {
        
        concurrentBufferQueue.sync {
            NSLog("Appending buffer :\(self.sessionStatus.buffers.count)")
            self.sessionStatus.buffers.append(outBuffer)
        }
    }
    func popBuffer()->AudioQueueBufferRef? {
        var tempBuffer:AudioQueueBufferRef? = nil
        concurrentBufferQueue.sync {
            tempBuffer = self.sessionStatus.buffers.popLast()!
        }
        return tempBuffer
    }
    
    func clearBuffer() {
        concurrentBufferQueue.sync {
            self.sessionStatus.buffers.removeAll()
        }
    }
    
    func deinitializeBuffer() {
        concurrentBufferQueue.sync {
            for abf:AudioQueueBufferRef? in sessionStatus.buffers {
                AudioQueueFreeBuffer(sessionStatus.queue!, abf!)
                abf?.deinitialize()
            }
            sessionStatus.buffers.removeAll()
            
        }
    }
    
    func openSession() ->Bool {
        NSLog("Opening session")
        accessory?.delegate = self
        self.eaSession = EASession(accessory: accessory!, forProtocol: eaProtocol!)
        if (self.eaSession != nil) {
            self.eaSession?.inputStream?.delegate = self
            self.eaSession?.inputStream?.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
            self.eaSession?.inputStream?.open()
            
            self.eaSession?.outputStream?.delegate = self
            self.eaSession?.outputStream?.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
            self.eaSession?.outputStream?.open()
        }
        else
        {
            NSLog("creating session failed");
        }
        return self.eaSession != nil
    }
    
    func closeSession() {
        self.eaSession?.inputStream?.close()
        self.eaSession?.inputStream?.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        self.eaSession?.inputStream?.delegate=nil
        
        self.eaSession?.outputStream?.close()
        self.eaSession?.outputStream?.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        self.eaSession?.outputStream?.delegate=nil
        self.eaSession = nil
        self.stop()
    }
    
    func readData() {
        NSLog("readData")
        if let inS = self.eaSession!.inputStream  {
            //find buffer
            while (inS.hasBytesAvailable) {
                //find nex free buffer
                if (sessionStatus.buffers.count <= 0) {
                    NSLog("Error no availabe buffer")
                    return
                }
                if let currentBuffer = popBuffer() {
                    let b = currentBuffer.pointee.mAudioData
                    //let b = UnsafeMutableRawPointer(currentBuffer.pointee.mAudioData)
                    //let ca = currentBuffer?.pointee.mAudioDataByteSize
                    let capacity = Int((currentBuffer.pointee.mAudioDataBytesCapacity))
                    //let buf = b?.assumingMemoryBound(to: UInt8.self)
                    let bu = b.assumingMemoryBound(to: UInt8.self)
                    let readed =     inS.read(bu, maxLength: capacity)
                    //let readed = self.eaSession?.inputStream?.read(b.assumingMemoryBound(to:UInt8.self), maxLength:capacity )
                    //NSLog("Readed :\(readed)")
                    if (readed > 0) {
                        currentBuffer.pointee.mAudioDataByteSize = UInt32(readed)
                        let nextBuffer:AudioQueueBufferRef = currentBuffer;
                        AudioQueueEnqueueBuffer(sessionStatus.queue!, nextBuffer, 0, nil)
                    }
                }
            }
            // NSNotificationCenter.defaultCenter().postNotificationName("ASDataReceived", object: self,userInfo: nil)
        }
    }
    
    func sendData(_ data:Data) {
        if _writeDate == nil {
            _writeDate = NSMutableData()
        }
        _writeDate?.append(data)
        self.writeData()
    }
    
    func writeData() {
        if let outS = self.eaSession!.outputStream {
            while(outS.hasSpaceAvailable && _writeDate != nil && _writeDate!.length > 0) {
                let toWrite = _writeDate!.bytes.bindMemory(to: UInt8.self, capacity: _writeDate!.length)
                let writen = eaSession?.outputStream?.write(toWrite, maxLength: _writeDate!.length)
                if writen < 0 {
                    NSLog("Write error")
                } else if (writen > 0) {
                    _writeDate?.replaceBytes(in: NSMakeRange(0, writen!), withBytes: nil, length: 0)
                }
            }
        }
    }
    
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        NSLog("Stream data event")
        switch eventCode {
        case Stream.Event():
            break;
        case Stream.Event.openCompleted:
            break;
        case Stream.Event.errorOccurred:
            break;
        case Stream.Event.endEncountered:
            break;
        case Stream.Event.hasSpaceAvailable:
            NSLog("Stream data event->HasSpaceAvailable")
            self.writeData()
            break;
        case Stream.Event.hasBytesAvailable:
            NSLog("Stream data event->HasBytesAvailable")
            self.readData()
            break;
        default:
            break;
        }
    }
    
    func stop() {
        if sessionStatus.playing {
            sessionStatus.playing = false
            AudioQueueStop(sessionStatus.queue!, true)
            //dealocate buffers
            deinitializeBuffer()
            AudioQueueDispose(sessionStatus.queue!, false)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ASPlayStoped"), object: nil)
        }
    }
    
    func play() {
        NSLog("PLaying")
        var status = AudioQueueNewOutput(&sessionStatus.format,
                                         audioStreamerOuputCallback,
                                         &sessionStatus,
                                         nil,
                                         nil,
                                         0, &sessionStatus.queue)
        if (status == 0) {
            sessionStatus.playing = true
            for _ in 0...ASEASessionController.BUFFERS_COUNT {
                var outBuffer:AudioQueueBufferRef? = nil
                AudioQueueAllocateBuffer(sessionStatus.queue!, ASEASessionController.BUFFER_SIZE, &outBuffer)
                if let bufferRef = outBuffer {
                    // configure audio buffer
                    bufferRef.pointee.mUserData = UnsafeMutableRawPointer(&sessionStatus)
                    bufferRef.pointee.mAudioDataByteSize = ASEASessionController.BUFFER_SIZE
                    appendBuffer(outBuffer: outBuffer!)
                }
            }
            status = AudioQueueStart(sessionStatus.queue!, nil)
            if (status == 0) {
                NSLog("Posting playing notofication")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ASPlayStarted"), object: self, userInfo: nil)
            }
        }
    }
    
    
    let audioStreamerOuputCallback: AudioQueueOutputCallback = {(userData,queueRef,outBuffer:AudioQueueBufferRef)->Void     in
        
        let state = ASEASessionController.sharedInstance.sessionStatus
        ASEASessionController.sharedInstance.appendBuffer(outBuffer: outBuffer)
        
        
    }
}
