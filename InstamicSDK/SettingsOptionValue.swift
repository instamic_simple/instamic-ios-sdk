//
//  SettingsOptionValue.swift
//  Instamic
//
//  Created by Jordan Kusic on 24.11.15..
//  Copyright © 2015. AdvancedSoft. All rights reserved.
//

import Foundation



public enum SettingsOptionValueType {
    case optionlist
    case onof
    case number
}

open class SettingsValue {
    open var label:String
    open var value:String
    open var selected:Bool
    open var onlyPro:Bool
    open var conditionalSelectable:[SettingsValue]?
    
    public init(forLabel label:String, andForValue value:String, andSelected selected:Bool,andOnlyForPro onlyPro:Bool,condition:[SettingsValue]) {
        self.label = label
        self.value = value
        self.selected = selected
        self.onlyPro = onlyPro
        conditionalSelectable = condition
    }
    public init(forLabel label:String, andForValue value:String, andSelected selected:Bool,andOnlyForPro onlyPro:Bool) {
        self.label = label
        self.value = value
        self.selected = selected
        self.onlyPro = onlyPro
        self.conditionalSelectable = nil
    }
    
    public init(forLabel label:String, andForValue value:String, andSelected selected:Bool) {
        self.label = label
        self.value = value
        self.selected = selected
        self.onlyPro = false
        self.conditionalSelectable = nil
    }
}

open class DeviceSettings {
    open var autoGain:Bool! = true
    open var gain:UInt8! = 0
    open var recordingMode:UInt8! = 0
    open var sampleRate:UInt8! = 0
    open var buzzer:Bool! = false
    open var remainingSpace:UInt32! = 0
    open var name:String = ""
    open var batteryLevel:UInt8 = 0;
}


open class SettingsOption {
    open var optionLable:String
    open var optionValueType:SettingsOptionValueType = SettingsOptionValueType.optionlist
    open var optionValues:[SettingsValue]
    open var optionName:String
    open var settings:DeviceSettings = DeviceSettings()
    
    public init(forName name:String, andForLabel label:String, valueType type:SettingsOptionValueType) {
        self.optionLable = label
        self.optionValues = []
        self.optionValueType = type
        optionName = name
    }
    init(forName name:String,  andForLabel label:String, valueType type:SettingsOptionValueType, andOptions options:[SettingsValue]) {
        self.optionLable = label
        self.optionValues = options
        self.optionValueType = type
        self.optionName = name
        
    }
    public func selectOneOption(_ name:String) {
        let maxIndex = self.optionValues.count - 1;
        for i in 0...maxIndex {
            let v:SettingsValue = self.optionValues[i];
            if (v.value == name) {
                v.selected = true
            } else {
                v.selected = false
            }
        }
    }
    public func getFirstSelected()->SettingsValue? {
        let maxIndex = self.optionValues.count - 1;
        for i in 0...maxIndex {
            let v:SettingsValue = self.optionValues[i];
            if (v.selected) {
                return v
            }
        }
        
        return nil;
    }
    public func getFirstNotSelected()->SettingsValue? {
        let maxIndex = self.optionValues.count - 1;
        for i in 0...maxIndex {
            let v:SettingsValue = self.optionValues[i];
            if (!v.selected) {
                return v
            }
        }
        
        return nil;
    }
    
    public func getFirstByValue(_ val:String) ->SettingsValue? {
        let maxIndex = self.optionValues.count - 1;
        for i in 0...maxIndex {
            let v:SettingsValue = self.optionValues[i];
            if (v.value == val) {
                return v
            }
        }
        
        return nil;
    }
    
    public func getFirst() -> SettingsValue {
        return self.optionValues[0];
    }
}


public protocol SettingsOptionsDelegate {
    func onSettingsOptionValueChanged(_ option:SettingsOption!,oldValue:SettingsValue!, newValue:SettingsValue!);
}
