//
//  InstamicDeviceManager.swift
//  InstamicSDK
//
//  Created by Jordan Kusic on 7/17/17.
//  Copyright © 2017 Jordan Kusic. All rights reserved.
//
import CoreBluetooth
import ExternalAccessory
import UIKit


public protocol InstamicDeviceManagerDelegate:class {
    
    func didStartSearching(manager:InstamicDeviceManager, cbcManager:CBCentralManager)
    func didDetectDevice(manager:InstamicDeviceManager,device:InstamicDevice)
    func didConnectDevice(manager:InstamicDeviceManager,device:InstamicDevice)
    func didDisconnectDevice(manager:InstamicDeviceManager,device:InstamicDevice)
    func didFailToConnect(manager:InstamicDeviceManager, cbcManager:CBCentralManager,peripheral: CBPeripheral, error: Error?)
    func onDeviceVUMeterValueRecived(manager:InstamicDeviceManager,device:InstamicDevice,value:CGFloat)
    func onDeviceSettingsUpdated(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceStoped(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceStarted(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceShutedDown(manager:InstamicDeviceManager,device:InstamicDevice)
    
    func onDeviceVUMeterDisabled(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceStreamEnabled(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceStatusUpdated(manager:InstamicDeviceManager,device:InstamicDevice,secondsFromStart:UInt32)
    func onDeviceRemainingSpaceUpdated(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceStartSemplingReceived(manager:InstamicDeviceManager,device:InstamicDevice)
    
    func onDeviceVersionInfoUpdated(manager:InstamicDeviceManager,device:InstamicDevice)
    func onDeviceBatteryLevelUpdated(manager:InstamicDeviceManager,device:InstamicDevice)
    
    func onDeviceAcessoryConnected(manager:InstamicDeviceManager,device:InstamicDevice)
    
    func onDeviceAcessoryDisonnected(manager:InstamicDeviceManager,device:InstamicDevice)
    
    
}

open class InstamicDeviceManager:NSObject, CBCentralManagerDelegate,CBPeripheralDelegate {
    
    let GO_DEV_UUID_SERVICE_PREFIX:CBUUID = CBUUID(string: "FF10")
    let PRO_DEV_UUID_SERVICE_PREFIX:CBUUID    = CBUUID(string: "FF11")
    static let EA_PROTOCOL = "com.instamic.transfer";
    
    var centralManager:CBCentralManager!
    var detectedDevices:[InstamicDevice] = []
    weak var delegate:InstamicDeviceManagerDelegate?
    var eaSessionOpened:Bool = false
    
    public convenience init(delegate:InstamicDeviceManagerDelegate) {
        self.init()
        self.delegate = delegate
        
    }
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InstamicDeviceManager.acessoryConnected), name:NSNotification.Name.EAAccessoryDidConnect , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InstamicDeviceManager.acessoryDisonnected), name:NSNotification.Name.EAAccessoryDidDisconnect , object: nil)
        EAAccessoryManager.shared().registerForLocalNotifications()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.EAAccessoryDidConnect , object: nil)
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name.EAAccessoryDidDisconnect , object: nil)
    }
    
    
    //cbc delegate methods
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            NSLog("Bluetooth turned power on")
            centralManager.scanForPeripherals(withServices: nil, options: nil)
            delegate?.didStartSearching(manager: self,cbcManager: central)
        }
        else {
            NSLog("Bluetooth switched off or not initialized")
        }
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        peripheral.delegate = self
        var isPro:Bool = false;
        let nameOfDeviceFound = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey) as? NSString
        let serviceUUIds:[CBUUID]? = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataServiceUUIDsKey) as? [CBUUID]
        let idetifielString = peripheral.identifier.uuidString
        NSLog("Device \(idetifielString) discovered")
        if serviceUUIds != nil && serviceUUIds!.count > 0{
            var found:Bool = false
            for ident in serviceUUIds! {
                if (ident == PRO_DEV_UUID_SERVICE_PREFIX) {
                    found = true;
                    isPro = true;
                    NSLog("Device \(idetifielString) is PRO")
                } else if (ident == GO_DEV_UUID_SERVICE_PREFIX) {
                    found = true;
                    NSLog("Device \(idetifielString) is GO")
                }
            }
            if (!found) {
                NSLog("Device \(idetifielString) is not Intamic GO nor PRO. Skipping")
                return
            }
        } else {
            return;
        }
        NSLog("Device name \(String(describing: nameOfDeviceFound))")
        var tempName:String?
        if (nameOfDeviceFound != nil) {
            tempName = nameOfDeviceFound as String?
        } else {
            tempName = "Unknowen";
        }
        if (getInstamicDeviceById(peripheral.identifier) == nil) {
            let device:InstamicDevice = InstamicDevice(id:peripheral.identifier as NSUUID, name: tempName,connected: false,pro:isPro);
            device.perhipheral = peripheral
            detectedDevices.append(device)
            delegate?.didDetectDevice(manager: self, device: device)
        } else {
            NSLog("Device \(idetifielString) already exists in list")
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        NSLog("Connected to the peripheral:\(peripheral.identifier.uuidString)")
        if let device = getInstamicDeviceById(peripheral.identifier) {
            device.connected = true;
            delegate?.didConnectDevice(manager: self, device: device)
            peripheral.discoverServices(nil)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?){
        NSLog("Disconected from :\(peripheral.identifier.uuidString) peripheral")
        if let device = removeInstamicDevice(peripheral.identifier) {
            device.connected = false;
            device.cmdCharactersitics = nil
            delegate?.didDisconnectDevice(manager: self, device: device)
            central.scanForPeripherals(withServices: nil, options: nil)
        }
        closeStream()
        //deviceDisconnectedHandler()
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        delegate?.didFailToConnect(manager: self, cbcManager: central, peripheral: peripheral, error: error)
    }
    
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        NSLog("Central manager w'll restore state")
    }
    
    
    //peripherl delegate methods
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        NSLog("Discovered characteristics")
        if let device = getInstamicDeviceById(peripheral.identifier) {
            for charateristic in service.characteristics! {
                let thisCharacteristic = charateristic as CBCharacteristic
                NSLog("Found characteristics UUID:\(thisCharacteristic.uuid) Start command")
                peripheral.setNotifyValue(true, for: charateristic)
                print("following properties:", terminator: "")
                device.cmdCharactersitics = thisCharacteristic
            }
            //ask for settings
            device.sendGetStatus()
        }
        centralManager.stopScan();
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        NSLog("Discovered service")
        for service in peripheral.services! {
            let thisService = service as CBService
            NSLog("UUID:\(service.uuid)")
            peripheral.discoverCharacteristics(nil, for: thisService)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        // NSLog("updated state for peripheral \(peripheral.identifier.UUIDString)");
        if let device = getInstamicDeviceById(peripheral.identifier) {
            //print("Data received")
            let dataBytes = characteristic.value
            let dataLength = dataBytes?.count
            var dataArray = [UInt8](repeating: 0, count: dataLength!)
            (dataBytes! as NSData).getBytes(&dataArray, length: dataLength! * MemoryLayout<UInt8>.size)
            // print(dataArray)
            var package = [UInt8]();
            let dataArayMaxIndex = dataArray.count - 1;
            for i in 1...dataArayMaxIndex {
                package.append(dataArray[i])
            }
            let decoded = InstamicDevice.cobsDecode(rawData: package)
            if (decoded.count > 3) {
                switch(decoded[2]) {
                case InstamicDevice.CMD_RESPONSE_OK:
                    self.handleResponseOk(device)
                    break;
                case InstamicDevice.CMD_RESPONSE_ERROR:
                    self.handleResponseError(device)
                    break;
                case InstamicDevice.CMD_VU_METER:
                    self.handleVuMeter(device,data:decoded)
                    break;
                case InstamicDevice.CMD_START:
                    self.handleStart(device,data:decoded)
                    break;
                case InstamicDevice.CMD_GET_SETTINGS:
                    self.handleSettings(device,data:decoded)
                    break
                case InstamicDevice.CMD_REMAINING_SPACE:
                    self.handleRemainingSpace(device,data:decoded)
                    break
                case InstamicDevice.CMD_START_SEMPLING:
                    self.handleStartSempling(device,data:decoded)
                    break
                case InstamicDevice.CMD_STOP:
                    self.handleStop(device,data:decoded)
                    break
                case InstamicDevice.CMD_GET_BAT_LEVEL:
                    self.handleBatteryLevel(device,data:decoded)
                    break
                case InstamicDevice.CMD_STATUS:
                    self.handleStatus(device,data:decoded)
                    break
                case InstamicDevice.CMD_VERSION_INFO:
                    self.handleVersionInfo(device,data:decoded)
                    break;
                case InstamicDevice.CMD_SHUTDOWN:
                    self.handleShutdown(device,data:decoded)
                    break;
                default: break
                    
                }
            } else {
                NSLog("Error:There is no device found in device list")
            }
        }
    }
    
    
    public func connectToDevice(device:InstamicDevice) {
        if centralManager.isScanning {
            centralManager.stopScan()
        }
        centralManager.connect(device.perhipheral!, options: nil)
    }
    
    public func disconnectFromDevice(device:InstamicDevice) {
        centralManager.cancelPeripheralConnection(device.perhipheral!)
    }
    
    
    public func refreshDeviceList() {
        if centralManager.isScanning {
            NSLog("refreshDeviceList->Stoping scanning")
            centralManager.stopScan()
        }
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        delegate?.didStartSearching(manager: self,cbcManager: centralManager)
    }
    
    //custom device  functions
    public func removeInstamicDevice(_ id:UUID) -> InstamicDevice? {
        var toremoveIndex:Int = -1;
        var counter:Int = -1;
        for dev in self.detectedDevices {
            counter += 1
            if (dev.id as UUID == id) {
                toremoveIndex = counter;
                break;
            }
        }
        if (toremoveIndex >= 0) {
            return detectedDevices.remove(at: toremoveIndex)
        }
        return nil
    }
    
    public func getInstamicDeviceById(_ id:UUID) -> InstamicDevice? {
        for dev in self.detectedDevices {
            if (dev.id as UUID == id) {
                return dev
            }
        }
        return nil
    }
    
    public func getFirstConnectedDevice() -> InstamicDevice? {
        for dev in self.detectedDevices {
            if (dev.connected) {
                return dev
            }
        }
        return nil
    }
    
    public func getConnectedDevices() -> [InstamicDevice?] {
        var result:[InstamicDevice?] = []
        for dev in self.detectedDevices {
            if (dev.connected) {
                result.append(dev)
            }
        }
        return result
    }
    
    
    public func getConnectedDeviceCount()-> Int {
        var counter:Int = 0
        for dev in self.detectedDevices {
            if (dev.connected) {
                counter += 1
            }
        }
        return counter
    }
    
    public func getDetectedDevices() -> [InstamicDevice?] {
        return detectedDevices
    }
    
    
    public func getDetectedDeviceCount()-> Int {
        return detectedDevices.count
    }
    
    
    //protocol communication handlers
    func handleResponseOk(_ device:InstamicDevice) {
        let lastCmd = device.lastCommandSent
        var lastPrm = device.lastParametersSent
        device.commandState = CommandState.IDLE
        device.onCommandExecuted();
        if (lastCmd == InstamicDevice.CMD_VU_METER) {
            if (lastPrm.count > 0 && lastPrm[0] == UInt8.min) {
                //vu meter is disabled
                delegate?.onDeviceVUMeterDisabled(manager: self, device: device)
            }
        }
        if lastCmd == InstamicDevice.CMD_STREAM_ENABLE {
            delegate?.onDeviceStreamEnabled(manager: self, device: device)
        }
    }
    
    func handleResponseError(_ device:InstamicDevice) {
        NSLog("Handling ERROR respone")
        device.onCommandError()
    }
    
    func handleBatteryLevel(_ device:InstamicDevice, data:[UInt8]) {
        device.commandState = CommandState.IDLE
        let value = data[3];
        NSLog("Battery level:\(value)")
        device.settings.batteryLevel = value
        delegate?.onDeviceBatteryLevelUpdated(manager: self, device: device)
        device.sendGetVersionInfo()
    }
    
    func handleVersionInfo(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        NSLog("Handling version info")
        device.mainVersionNumber = data[3]
        device.mediumVersionNumber = data[4]
        device.minorVersionNumber = data[5]
        delegate?.onDeviceVersionInfoUpdated(manager: self, device: device)
    }
    
    func handleShutdown(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        delegate?.onDeviceShutedDown(manager: self, device: device)
        NSLog("Shuting down device")
    }
    
    func handleStatus(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        let recording = data[3];
        let autoGain = data[4];
        let streaming = data[5];
        NSLog("REC:\(recording) Auto Gain:\(autoGain) streaming:\(streaming)")
        if (recording > 0) {
            device.recordingState = RecordingState.RECORDING
        }
        if (streaming > 0) {
            device.recordingState = RecordingState.MONITORING
            device.streamingEnabled = true;
        }
        var secondsFromStart:UInt32 = 0
        if streaming > 0 || recording > 0 {
            let param : [UInt8] = [data[6], data[7],0,0]
            secondsFromStart = UnsafePointer(param).withMemoryRebound(to: UInt32.self, capacity: 1){
                $0.pointee
            }
            NSLog("Saving seconds:\(secondsFromStart) .Start sampling")
            
        } else {
            device.recordingState = RecordingState.IDLE
        }
        delegate?.onDeviceStatusUpdated(manager: self, device: device, secondsFromStart: secondsFromStart)
        if (device.streamingEnabled) {
            openStream(play: true)
        }
        device.sendGetSettings()
    }
    
    
    func handleRemainingSpace(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        let param : [UInt8] = [data[3], data[4], data[5], 0]
        let value = UnsafePointer(param).withMemoryRebound(to: UInt32.self, capacity: 1){
            $0.pointee
        }
        NSLog("Remaining space:\(value)")
        device.settings.remainingSpace = value
        delegate?.onDeviceRemainingSpaceUpdated(manager: self, device: device)
        device.sendGetBatterylevel()
    }
    
    
    
    func handleStartSempling(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        NSLog("Start sampling device:\(device.id.uuidString)");
        let autoGain = data[3];
        
        if autoGain == UInt8.max {
            device.recordingState = RecordingState.SETTING_AUTOGAIN
            delegate?.onDeviceStartSemplingReceived(manager: self, device: device)
            return
        }
        
        if device.streamingEnabled {
            device.recordingState = RecordingState.MONITORING
        } else {
            device.recordingState = RecordingState.RECORDING
        }
        delegate?.onDeviceStartSemplingReceived(manager: self, device: device)
        if device.streamingEnabled && eaSessionOpened {
            ASEASessionController.sharedInstance.play()
        }
    }
    
    func handleStop(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        NSLog("Stoping device:\(device.id.uuidString)");
        device.recordingState = RecordingState.IDLE
        
        closeStream()
        if device.streamingEnabled {
            device.streamDisable()
        } else {
            device.sendDisableVuMetter()
        }
        delegate?.onDeviceStoped(manager: self, device: device)
        
    }
    
    func handleStart(_ device:InstamicDevice,data:[UInt8]) {
        NSLog("Starting device \(String(describing: device.name)) recording");
        device.recordingState = RecordingState.PREPARING
        if device.streamingEnabled {
            openStream(play: false)
        }
        device.sendEnableVuMetter()
        delegate?.onDeviceStarted(manager: self, device: device)
    }
    
    func handleSettings(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        let autoGain = data[3];
        let gain = data[4];
        let recordngMode = data[5];
        let sampleRate = data[6];
        let buzzer = data[7];
        let filter = data[8];
        
        if (autoGain < 1) {
            device.settings.autoGain = false
        } else {
            device.settings.autoGain = true;
        }
        device.settings.gain = gain
        device.settings.recordingMode = recordngMode
        device.settings.sampleRate = sampleRate
        if (buzzer < 1) {
            device.settings.buzzer = false
        } else {
            device.settings.buzzer = true
        }
        NSLog("Settings AG:\(autoGain) G:\(gain) RM:\(recordngMode) SR:\(sampleRate) BUZZ:\(buzzer) F:\(filter)")
        delegate?.onDeviceSettingsUpdated(manager: self, device: device)
        
        device.sendGetRemainingSpace();
    }
    
    func handleVuMeter(_ device:InstamicDevice,data:[UInt8]) {
        device.commandState = CommandState.IDLE
        let param: [UInt8] = [data[3], data[4], data[5], 0]
        let value = UnsafePointer(param).withMemoryRebound(to: UInt32.self, capacity: 1){
            $0.pointee
        }
        var res:UInt8 = 24;
        if (device.streamingEnabled) {
            res = 16;
        }
        let v = InstamicDevice.sampletoToPct(sample: value, resolution: res);
        delegate?.onDeviceVUMeterValueRecived(manager: self, device: device, value: CGFloat(v))
    }
    
    //stream functions
    func closeStream() {
        NSLog("closeStream")
        if (eaSessionOpened) {
            ASEASessionController.sharedInstance.closeSession()
            eaSessionOpened = false
        }
    }
    
    public func countStreamEnabledDevice()->Int {
        var count = 0;
        for acc in EAAccessoryManager.shared().connectedAccessories {
            if (acc.protocolStrings.contains(InstamicDeviceManager.EA_PROTOCOL)) {
                count += 1
            }
        }
        return count
    }
    
    func openStream(play:Bool) {
        NSLog("openStream")
        if eaSessionOpened == false {
            NSLog("Opening session")
            for acc in EAAccessoryManager.shared().connectedAccessories {
                if (acc.protocolStrings.contains(InstamicDeviceManager.EA_PROTOCOL)) {
                    ASEASessionController.sharedInstance.settupForAccessory(acc, eaP: InstamicDeviceManager.EA_PROTOCOL)
                    NSLog("Setteed up accessory for stream")
                    eaSessionOpened = ASEASessionController.sharedInstance.openSession()
                    if (play) {
                        ASEASessionController.sharedInstance.play()
                    }
                    NSLog("Session open for stream")
                }
            }
        }
    }
    
    func acessoryConnected(_ notification:Notification) {
        NSLog("Accessory connected")
        if let userInfo = notification.userInfo {
            let acc = userInfo[EAAccessoryKey] as! EAAccessory
            if (acc.protocolStrings.contains(InstamicDeviceManager.EA_PROTOCOL)) {
                ASEASessionController.sharedInstance.settupForAccessory(acc, eaP: InstamicDeviceManager.EA_PROTOCOL)
                NSLog("Setteed up accessory for stream")
                if let device:InstamicDevice = getFirstConnectedDevice() {
                    delegate?.onDeviceAcessoryConnected(manager: self, device: device)
                    if !device.streamingEnabled {
                        return;
                    }
                    eaSessionOpened = ASEASessionController.sharedInstance.openSession()
                    if eaSessionOpened {
                        ASEASessionController.sharedInstance.play()
                    }
                    NSLog("Session open for stream")
                }
                
            }
        }
    }
    
    func acessoryDisonnected(_ notification:Notification) {
        NSLog("Accessory disconnected")
        if let userInfo = notification.userInfo {
            let acc = userInfo[EAAccessoryKey] as! EAAccessory
            if (acc.protocolStrings.contains(InstamicDeviceManager.EA_PROTOCOL)) {
                if (eaSessionOpened) {
                    (ASEASessionController.sharedInstance.closeSession())
                }
            }
        }
    }
    
}
